CONTENTS OF THIS FILE
------------

  * Introduction
  * Dependencies
  * Installation
  * Credits
  
Introduction
------------

This module enables you to get and show purolator shipping cost estimates on 
checkout of Drupal Commerce orders. The estimates are based on the location of
 shipper, ship to location, physical weight of the order and type of packaging.

Dependencies
------------
Physical (physical fields - http://drupal.org/project/physical)
Commerce shipping (including �Shipping UI� - 
 http://drupal.org/project/commerce_shipping)
Commerce (http://drupal.org/project/commerce)


Installation
------------

To use this module, the products on your site must have a weight field. The
 module �Physical�; mentioned above provides physical fields. Apply for 
 Purolator API credentials at Purolator.com. To add the weight field to your
 product, follow these steps.

1. Go to Home � Administration � Store � Products.
2. Click the �product types� tabs of the overlay.
3. Click the �manage fields� link and add a field with machine name 
�field_weight� and use the field type �Physical weight�. The widget for this
 field would be �Weight textfield�.
4. Save the field and then the product type.
5. Install and enable the Purolator module and the dependency modules. You 
would find the Purolator in �commerce (shipping)� section of modules list.
6. After installation you would see message on checkout page that will provide
 you direct link to configure the Purolator. Otherwise, you may find 
 configurations form in this location �Store � Configuration � Shipping� and 
 click �edit� in front of Purolator.
7. To set up destinations for you shipments using Purolator, click the link
 provided on this form. Please provide a Location for Canada, a location for US
 and a location for any other place. Examples are provided at bottom of the 
 document.
8. Download wsdl file from Purolator when you register your self on purolator.
 Then add the wsdl in libraries folder with name "commerce_purolator_shipping" 
 and in this folder copy wsdl file like this commerce_purolator_shipping/dev/*
 commerce_purolator_shipping/prod/*.
 
Credits
------------

This module is developed by Cognitive Axis (cognitiveaxis.com) and sponsored by
The Creadtion Studio (http://www.creationstudio.ca/)

For any help please email kamran.zafar@cognitiveaxis.com or Skype kamran_z
